#!/bin/bash

#    This file is part of vlbuildbot.
#
#    vlbuildbot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vlbuildbot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.

cd $PWD
CWD=$(pwd)
export REPO=$(pwd)
listing=$(git show --pretty="" --name-only HEAD)

for f in ${listing}; do
	if [[ "$f" == *.SlackBuild ]]; then
		fpath=$PWD/$f
		sbname=$(basename $f)
		# Build it
		(
		cd $(dirname $fpath) || exit 1
		echo "[ VLDEPPER ]:  Using REPO path $REPO"
		vldepper $sbname || exit 1
		sh $sbname || exit 1
		) || exit 1
	fi
done
