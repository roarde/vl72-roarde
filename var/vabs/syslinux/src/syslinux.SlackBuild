#!/bin/sh

# Copyright 2006, 2007, 2008, 2009, 2010, 2012  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME=syslinux
VERSION=${VERSION:-4.06}
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
LINK=${LINK:-"http://www.kernel.org/pub/linux/utils/boot/$NAME/$NAME-$VERSION.tar.xz"}
MAKEDEPENDS=${MAKEDEPENDS:-"nasm gptfdisk efibootmgr"}


if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
  CONFIGURE_TRIPLET="i586-vector-linux"
#  _targets="bios efi32"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
#  _targets="bios efi32 efi64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-${NAME}
rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf ${NAME}-${VERSION}
tar xvf $CWD/${NAME}-$VERSION.tar.xz || exit 1
cd ${NAME}-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find . \
  \( -perm 2777 -o -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;
CFLAGS=
CXXFLAGS=

#make clean
#make installer
# Build:
make $NUMJOBS || exit 1

# Without -i, the following fails trying to install w32 junk:
make -i install INSTALLROOT=$PKG SBINDIR=/usr/sbin MANDIR=/usr/man AUXDIR=/usr/lib/syslinux || exit 1

cp -a linux/syslinux-nomtools $PKG/usr/bin
chmod 755 $PKG/usr/bin/*

rm -rf $PKG/usr/share/syslinux/com32

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory:
mkdir -p $PKG/usr/doc/${NAME}-$VERSION/{cmenu,com32}
cp -a \
  BUGS COPYING NEWS README* TODO \
  doc \
  $PKG/usr/doc/${NAME}-$VERSION
cp -a com32/LICENCE $PKG/usr/doc/${NAME}-$VERSION/com32
( cd com32/cmenu
  cp -a \
    CHANGES HISTORY MANUAL MENU_FORMAT README TODO \
    $PKG/usr/doc/${NAME}-$VERSION/cmenu
)

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VER-$ARCH-$BUILD.txz
rm -rf $TMP
