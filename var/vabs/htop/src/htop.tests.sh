#!/bin/bash

# Test to make sure the pkg produced a binary
printf "Test for program binary ... "
[ -x /usr/bin/htop ] || { printf "FAILED\n"; exit 1; }
printf "PASS \n"
