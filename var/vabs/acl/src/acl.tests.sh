#!/bin/bash

check_file_exists() {
	file="$1"
	[ -f $file ] || return 1
}

secho() {
	msg="$1"
	printf "$msg .... "
}

eecho() {
	msg="$1"
	printf "$msg\n"
}

for file in /usr/bin/getfacl /usr/bin/setfacl /usr/bin/chacl ; do
	secho " + Testing file $file exists"
	check_file_exists $file || { eecho "FAILED"; exit 1; }
	eecho "PASSED"
done

secho " + Test other programs that need acl "
echo ""
secho " + Testing sed"
sed --help >/dev/null || { eecho "FAILED"; exit 1; }
eecho "PASSED"
secho " + Testing 'ls'"
ls --help >/dev/null || { eecho "FAILED"; exit 1; }
eecho "PASSED"
