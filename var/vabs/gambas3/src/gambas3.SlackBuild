#!/bin/bash

##SlackBuild style build script

NAME=gambas3
VERSION=3.2.1
ARCH=$(uname -m)
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
VL_PACKAGER=Uelsk8s
SOURCE="http://prdownloads.sourceforge.net/gambas/$NAME-$VERSION.tar.bz2"
MAKEDEPENDS=${MAKEDEPENDS:-"slapt-get openssl curl"}

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME



if [ "$TMP" = "" ]; then
  TMP=/tmp
fi

if [ $UID != 0 ]; then
	echo "You need to be root to run this script."
	exit
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
	echo "Requiredbuilder not installed, or not executable."
	#exit
fi

if [ ! -d $TMP/finished-packages ]; then
	mkdir -p $TMP/finished-packages
fi

PKG=$TMP/package-$NAME
SRCDIR=$TMP/$NAME-$VERSION
rm -rf $PKG
rm -rf $PKG-runtime
rm -rf $SRCDIR
mkdir -p $PKG/usr
mkdir $PKG/install
mkdir -p $PKG-runtime/usr
mkdir $PKG-runtime/install


# Get the source
if [ ! -f $CWD/$NAME-$VERSION.tar.bz2 ]; then
	wget -c $SOURCE 
fi

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=x86
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"

tar xvf $CWD/$NAME-$VERSION.tar.bz2 -C $TMP
cd $SRCDIR
chown -R root:root .
./reconf-all

CPPFLAGS='-I/usr/include/poppler -I/usr/X11R6/include' \
LDFLAGS='-L/usr/X11R6/lib' \
./configure --prefix=/usr \
--libdir=/usr/lib${LIBDIRSUFFIX} \
--with-moc=/opt/kde3/bin/moc --sysconfdir=/etc \
--with-qt-dir=/opt/kde3/lib${LIBDIRSUFFIX}/qt3 \
--with-qt-includes=/opt/kde3/lib${LIBDIRSUFFIX}/qt3/include \
--with-qt-libraries=/opt/kde3/lib${LIBDIRSUFFIX}/qt3/lib \
--with-extra-includes=/opt/kde3/include \
--mandir=/usr/man \
--localstatedir=/var \
--with-included-gettext \
--enable-bzlib2 \
--enable-zlib2 \
--enable-mysql \
--enable-sqlite2 \
--enable-sqlite3 \
--enable-postgresql \
--enable-gtk \
--enable-gtksvg \
--enable-pdf \
--enable-net \
--enable-curl \
--enable-smtp \
--enable-pcre \
--enable-qt \
--enable-qte \
--enable-sdl \
--enable-sdlsound \
--enable-xml \
--enable-v4l \
--enable-crypt \
--enable-opengl \
--enable-ldap \
--enable-image \
--enable-kde \
--enable-optimization \
--disable-debug

sleep 5 # Just a wait to see if any components were not built
make

if ( ! make ); then
	echo "make failed"
	exit
fi

cp $CWD/slack-desc* .

cat >> slack-desc_ide << EOF

#----------------------------------------

BUILDDATE: `date`
PACKAGER:  $VL_PACKAGER
HOST:      `uname -srm`
DISTRO:    `cat /etc/vector-version`
CFLAGS:    $CFLAGS
CONFIGURE: `awk "/\.\/configure\ /" config.log`

EOF

cat >> slack-desc_runtime << EOF
#----------------------------------------

BUILDDATE: `date`
PACKAGER:  $VL_PACKAGER
HOST:      `uname -srm`
DISTRO:    `cat /etc/vector-version`
CFLAGS:    $CFLAGS
CONFIGURE: `awk "/\.\/configure\ /" config.log`

EOF


make install DESTDIR=$PKG
mkdir -p $PKG/usr/share/applications
mkdir -p $PKG/opt/kde/share/applnk/Development/Gambas/Docs
mkdir $PKG/usr/share/pixmaps
mkdir $PKG-runtime/usr/bin
mkdir -p $PKG-runtime/usr/lib${LIBDIRSUFFIX}
mkdir -p $PKG-runtime/usr/share/gambas3
mkdir -p $PKG/install
mkdir -p $PKG-runtime/install

# This is just a trick to trim out a bunch of junk off these desc files.
cat slack-desc_ide | grep -v "^configure:[0-9]" > $PKG/install/slack-desc
cat slack-desc_runtime | grep -v "^configure:[0-9]" > $PKG-runtime/install/slack-desc

cat $CWD/gambas3.desktop > $PKG/usr/share/applications/gambas3.desktop
cat $CWD/gambas3.desktop.kde > \
	$PKG/opt/kde/share/applnk/Development/Gambas/gambas3.desktop
cat $CWD/manual.desktop.kde > \
	$PKG/opt/kde/share/applnk/Development/Gambas/Docs/manual.desktop
cat $CWD/gambas.png > $PKG/usr/share/pixmaps/gambas.png
cat $CWD/gambas3.png > $PKG/usr/share/pixmaps/gambas3.png


( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

#Move some files out to the runtime packaging directory
mv $PKG/usr/share/gambas3/info $PKG-runtime/usr/share/gambas3
mv $PKG/usr/lib${LIBDIRSUFFIX}/gambas3 $PKG-runtime/usr/lib${LIBDIRSUFFIX}
rmdir $PKG/usr/lib${LIBDIRSUFFIX}
mv $PKG/usr/bin/gbi3 $PKG-runtime/usr/bin
mv $PKG/usr/bin/gbx3 $PKG-runtime/usr/bin
mv $PKG/usr/bin/gba3 $PKG-runtime/usr/bin
mv $PKG/usr/bin/gbc3 $PKG-runtime/usr/bin

mkdir -p $PKG/usr/doc/$NAME-$VERSION
mkdir -p $PKG-runtime/usr/doc/$NAME-runtime-$VERSION
cp -a \
	AUTHORS COPYING ChangeLog NEWS README* TODO \
	$PKG/usr/doc/$NAME-$VERSION
cp -a \
	AUTHORS COPYING ChangeLog NEWS README* TODO \
	$PKG-runtime/usr/doc/$NAME-runtime-$VERSION

cat $PKG/install/slack-desc > $RELEASEDIR/slack-desc_ide
cat $PKG-runtime/install/slack-desc > $RELEASEDIR/slack-desc_runtime

#cat $CWD/slack-desc_ide > $PKG/install/slack-desc
#cat $CWD/slack-desc_runtime > $PKG-runtime/install/slack-desc
requiredbuilder -v -y -s $RELEASEDIR $PKG-runtime
requiredbuilder -v -y -s $RELEASEDIR $PKG
#cat $CWD/slack-required > $PKG/install/slack-required
#cat $CWD/slack-required > $PKG-runtime/install/slack-required
cat $CWD/slack-suggests > $PKG/install/slack-suggests
cat $CWD/slack-suggests > $PKG-runtime/install/slack-suggests
echo "$NAME-runtime >= $VERSION-$ARCH-$BUILD" >> $PKG/install/slack-required

cd $PKG
#makepkg -p -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.tlz
makepkg -p -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz
cd $PKG-runtime
#makepkg -p -l y -c n $RELEASEDIR/$NAME-runtime-$VERSION-$ARCH-$BUILD.tlz
makepkg -p -l y -c n $RELEASEDIR/$NAME-runtime-$VERSION-$ARCH-$BUILD.txz
#cp $PKG/$NAME-$VERSION-$ARCH-$BUILD.tlz $TMP/finished-packages
#echo "$NAME-$VERSION package is in $TMP/finished-packages"
#cp $NAME-runtime-$VERSION-$ARCH-$BUILD.tlz $TMP/finished-packages
#echo "$NAME-runtime-$VERSION package is in $TMP/finished-packages"
rm -rf $TMP
rm -rf $SRCDIR
rm -rf $PKG
rm -rf $PKG-runtime
