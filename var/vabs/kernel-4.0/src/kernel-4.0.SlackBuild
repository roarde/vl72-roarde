#!/bin/bash

# ----------------------
VER=4.0.7
KERNEL=linux-$VER
AUFS_BRANCH="origin/aufs4.0"
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=$(uname -m)
LINK=${LINK:-"http://www.kernel.org/pub/linux/kernel/v4.x/$KERNEL.tar.xz"}
NUMJOBS=${NUMJOBS:-"-j`nproc`"}
MAKEDEPENDS="bc"
# ----------------------

if [ "$NORUN" != 1 ]; then

eecho()
{
   echo "-------------------------------------"
   echo " * " "$@"
   echo "-------------------------------------"
}

#set -e
CURDIR=$(pwd)
TMP="/usr/src/"
cd ../
PKG=$(pwd)
cd $CURDIR
#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
  if [ ! -f $(basename $SRC) ];then
    wget --no-check-certificate -c $SRC
  fi
done
#--------------------------------------------
#rm -rf $TMP/Makefile* $TMP/$AUFS $TMP/$LZMA $TMP/$SQUASHFS $TMP/$KERNEL
rm -rf $TMP/Makefile* $TMP/$KERNEL
mkdir -p $TMP
rm -Rf $PKG/{kernel,modules,pkgsrc,headers}
mkdir -p $PKG/{kernel,modules,pkgsrc,headers}/install
##setup package dirs
cat >> $PKG/kernel/install/doinst.sh << EOF
if [ ! -f /boot/vmlinuz ];then
( cd boot ; rm -rf System.map )
( cd boot ; ln -sf System.map-$VER System.map )
( cd boot ; rm -rf config )
( cd boot ; ln -sf config-$VER config )
( cd boot ; ln -sf vmlinuz-$VER vmlinuz )
fi

EOF
cat >> $PKG/kernel/install/slack-desc << EOF
kernel: VectorLinux $KERNEL kernel
kernel:
kernel: Built for Vectorlinux
kernel: based on the config-2.6.19.2-VLsleep
kernel: config by joe1962
kernel:
kernel:
kernel:
kernel:
kernel:
kernel: Package Created By: Uelsk8s

EOF
cat >> $PKG/modules/install/doinst.sh << EOF

depmod -aq $VER

EOF
cat >> $PKG/modules/install/slack-desc << EOF
kernel-modules: VectorLinux $KERNEL kernel modules
kernel-modules:
kernel-modules: These are Linux kernel modules (device drivers)
kernel-modules: 
kernel-modules: config by joe1962 
kernel-modules: 
kernel-modules: 
kernel-modules:  
kernel-modules: 
kernel-modules: 
kernel-modules: built by Uelsk8s for Vectorlinux

EOF
cat >> $PKG/pkgsrc/install/doinst.sh << EOF
#!/bin/bash

EOF
cat >> $PKG/pkgsrc/install/slack-desc << EOF
kernel-src: VectorLinux $KERNEL kernel source
kernel-src:
kernel-src: Built for Vectorlinux
kernel-src:
kernel-src: based on the config-2.6.19.2-VLsleep
kernel-src: by joe1962
kernel-src:
kernel-src:
kernel-src:
kernel-src:
kernel-src: Package Created By: Uel Archuletta

EOF
cat >>$PKG/headers/install/slack-desc << EOF
kernel-headers: VectorLinux kernel-$KERNEL headers
kernel-headers:
kernel-headers: Contains the include files from the built kernel source
kernel-headers: from kernel $KERNEL
kernel-headers:
kernel-headers:

EOF

##start
cd $TMP
K=`echo $TMP/$KERNEL | sed -r s/'\/'/'\\\\\/'/g`

function unpackKernel()
{
   # when the kernel version exist, simply untar:
   if [ -e $CURDIR/$KERNEL.tar.xz ]; then
      # Spare the looooooooong output during decompression
      tar xf $CURDIR/$KERNEL.tar.xz --checkpoint=10000 || exit 1
   else # a -rc kernel is used, we must unpack older version and apply -rc patch
      wget http://www.kernel.org/pub/linux/kernel/v4.x/$KERNEL.tar.xz 
      mv $KERNEL.tar.xz $CURDIR
      tar -xf $CURDIR/$KERNEL.tar.xz --checkpoint=10000 || exit 1
   fi
}

eecho "Unpacking archives"

echo $KERNEL
unpackKernel $KERNEL
ln -s $KERNEL ./linux

eecho "Preparing kernel: aufs performance patches, squashfs + lzma patches"

## BEGIN AUFS PATCH
( cd $CURDIR
mkdir patches
cd patches
git clone git://github.com/sfjro/aufs4-standalone.git
cd aufs4-standalone
git checkout ${AUFS_BRANCH} || exit 1
)
cd $TMP/$KERNEL
# Copy the source for the aufs support into the kernel
cp -f $CURDIR/patches/aufs4-standalone/include/uapi/linux/aufs_type.h $TMP/$KERNEL/include/uapi/linux/aufs_type.h || exit 1
cp -rf $CURDIR/patches/aufs4-standalone/{Documentation,fs} $TMP/$KERNEL || exit 1
# Apply the patches
patch -Np1 --verbose <$CURDIR/patches/aufs4-standalone/aufs4-base.patch || exit 1
patch -Np1 --verbose <$CURDIR/patches/aufs4-standalone/aufs4-mmap.patch || exit 1
patch -Np1 --verbose <$CURDIR/patches/aufs4-standalone/aufs4-standalone.patch || exit 1
patch -Np1 --verbose <$CURDIR/patches/aufs4-standalone/aufs4-kbuild.patch || exit 1
patch -Np1 --verbose <$CURDIR/patches/aufs4-standalone/aufs4-loopback.patch || exit 1
patch -Np1 --verbose <$CURDIR/patches/aufs4-standalone/tmpfs-idr.patch || exit 1
patch -Np1 --verbose <$CURDIR/patches/aufs4-standalone/vfs-ino.patch || exit 1
## END AUFS PATCH
eecho "configuring kernel"

if [ $ARCH == x86_64 ];then
  cp $CURDIR/config-64 .config
elif [ $ARCH == "i686" ];then
  ARCH=x86
  cp $CURDIR/config-32 .config
else
  echo "$ARCH Unsuported Arch"
  exit 1
fi
make oldconfig || exit 1
make scripts || exit 1
make prepare || exit 1

eecho "compiling kernel"
make $NUMJOBS modules || exit 1
INSTALL_MOD_PATH=$PKG/modules make modules_install || exit 1
# Install the kernel headers
cp .config $CURDIR/config.used-$ARCH
make headers_check || exit 1
make INSTALL_HDR_PATH=dest headers_install || exit 1
find dest/include \( -name .install -o -name ..install.cmd \) -delete
mkdir -p $PKG/headers/usr/include || exit 1
cp -rv dest/include/* $PKG/headers/usr/include/ || exit 1
rm -rf dest
( cd $PKG/headers
mv usr/include/asm usr/include/asm-x86
cd usr/include
rm -rf asm
ln -sf asm-x86 asm
) || exit 1
make $NUMJOBS bzImage || exit 1


cd $TMP/$KERNEL
mkdir -p $PKG/kernel/boot
cp .config $PKG/kernel/boot/config-$VER
if [ $ARCH == x86_64 ];then
	cp arch/x86/boot/bzImage $PKG/kernel/boot/vmlinuz-$VER
else
	cp arch/i386/boot/bzImage $PKG/kernel/boot/vmlinuz-$VER
fi
cp System.map $PKG/kernel/boot/System.map-$VER

eecho "Building Packages"
#build packages
cd $PKG/kernel
makepkg -l y -c n $PKG/kernel-$VER-$ARCH-$BUILD.txz
# Build the kernel-headers package
cd $PKG/headers
makepkg -l y -c n $PKG/kernel-headers-$VER-$ARCH-$BUILD.txz

cd ../modules
makepkg -l y -p -c n $PKG/kernel-modules-$VER-$ARCH-$BUILD.txz

##Starting driver builds
installpkg $PKG/kernel-modules-*
installpkg $PKG/kernel-headers-$VER-$ARCH-$BUILD.txz
( cd /usr/src && rm linux; ln -s $KERNEL linux )
cd $CURDIR
for i in $(cat list);do
	cp -ax /home/slackbuilds/var/vabs/$i /tmp/builds/
	cd /tmp/builds/$i/src/
	KBUILD=/usr/src/linux-$VER KVERS=$VER KERNEL=$VER sh $i.SlackBuild
done

eecho "cleaning Source tree"
cd $PKG/pkgsrc
mkdir -p usr/src
cp -ax $TMP/$KERNEL usr/src
( cd usr/src && ln -s $KERNEL linux )
mkdir -p usr/src/build-$VER
cp usr/src/$KERNEL/.config $CURDIR/config
for i in $CURDIR/*;do if ! [ -d $i ];then cp -ax $i usr/src/build-$VER;fi;done
rm -rvf usr/src/build-$VER/linux-$VER*
cp -ax $CURDIR/patches usr/src/build-$VER
rm -v usr/src/$KERNEL/arch/x86/boot/bzImage
rm -v usr/src/$KERNEL/arch/x86/boot/vmlinux.bin
rm -v usr/src/$KERNEL/arch/x86/boot/compressed/vmlinux
rm -v usr/src/$KERNEL/arch/x86/boot/compressed/vmlinux.bin
rm -v usr/src/$KERNEL/arch/x86/boot/compressed/vmlinux.bin.*
rm -v usr/src/$KERNEL/vmlinux
makepkg -l y -c n $PKG/kernel-src-$VER-$ARCH-$BUILD.txz

eecho "cleaning Crippled source tree"
# build crippled kernel source package
cd usr/src/$KERNEL
# remove a lot of stuff
for i in Documentation drivers fs net sound;do
  echo "removing $i"
  rm -Rf $i
done


ls -1d arch/* | grep -v x86 | xargs rm -Rf
ls -1d include/* | grep asm- | grep -v x86 | grep -v generic | xargs rm -Rf
cd ..
rm -r build*

cd $PKG/pkgsrc
cat > $PKG/pkgsrc/install/slack-desc << EOF
kernel-src: VectorLinux $KERNEL kernel source
kernel-src: stripped kernel source
kernel-src: Built for Vectorlinux
kernel-src: This is a stripped down kernel source 
kernel-src: tree. It is INCOMPLETE!
kernel-src: It's used if you build a kernel modules
kernel-src: for your devices, but it can't be used 
kernel-src: to build completely new kernel.
kernel-src: based on the config-2.6.19.2-VLsleep
kernel-src: by joe1962
kernel-src: Package Created By: Uel Archuletta


EOF
cat > usr/src/$KERNEL/README.IMPORTANT << EOFF
This is a stripped down kernel source tree. It is INCOMPLETE!
It's used if you build a kernel modules for your devices,
but it can't be used to build completely new kernel.

If you need full kernel sources, detele all the directories here 
and get the sources from http://www.kernel.org/
EOFF

makepkg -l y -c n $PKG/kernel-stripped-src-$VER-$ARCH-$BUILD.txz

cd ../
rm -r kernel modules pkgsrc
echo packages are finished

fi
