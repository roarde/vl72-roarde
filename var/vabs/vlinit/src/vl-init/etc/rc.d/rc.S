#!/bin/sh
# rc.S:  System initialization script that runs at the
#        very beginning of booting.
#
# Originally from Slackware 10.0
# Mostly written by:  Patrick J. Volkerding, <volkerdi@ftp.cdrom.com>
#
# Adapted for Vector Linux by 
# Eko M. Budi, Robert S. Lange, Tony Brijeski, Uel Archuletta(bootsplash-text)
#

. /etc/rc.d/functions-display

PATH=/sbin:/usr/sbin:/bin:/usr/bin

dmesg -n 1

# bootsplash
. /etc/rc.d/rc.bootsplash
# start animation to rc.S
animate startup

#Set YES if you want a "rainbow-technicolor" initialization
export  RCINITCOLORS="YES" 

echoc "==> rc.S  Setting up the base system configuration" yellow
echoc "Initializing kernel file systems" cyan

# Mount /proc right away:
echo "Mounting /proc file system" 
/sbin/mount -v proc /proc -n -t proc

# Mount sysfs next, if the kernel supports it:
if [ -d /sys ]; then
  if cat /proc/filesystems | grep -w sysfs 1> /dev/null 2> /dev/null ; then
    if ! cat /proc/mounts | grep -w sysfs 1> /dev/null 2> /dev/null ; then
      /etc/rc.d/rc.text 2 'Starting sysfs'
      # bootsplash
      progressbar 5
      echo "Starting sysfs"
      /sbin/mount -v sysfs /sys -n -t sysfs
    fi
  fi
fi

  
# Start file systems
     /etc/rc.d/rc.text 2 'Mounting local file systems'
      # bootsplash
      progressbar 10
echoc "Mounting local file systems" cyan

# Test to see if the root partition is read-only, like it ought to be.
READWRITE=no

if touch /fsrwtestfile 2>/dev/null; then
  rm -f /fsrwtestfile
  READWRITE=yes
else
  /etc/rc.d/rc.text 3 "Testing root filesystem status"
      # bootsplash
      progressbar 15
  echo "Testing root filesystem status:  read-only filesystem"
fi

# See if a forced filesystem check was requested at shutdown:
if [ -r /etc/forcefsck ]; then
  FORCEFSCK="-f"
fi


# Check the root filesystem:
if [ ! $READWRITE = yes ]; then
  if [ ! -r /etc/fastboot ]; then
    /etc/rc.d/rc.text 3 "Checking root filesystem"
      # bootsplash
      progressbar 15
    echo "Checking root filesystem"
    /sbin/fsck $FORCEFSCK -C -a /
    RETVAL=$?
  fi

# An error code of 2 or higher will require a reboot.
  if [ $RETVAL -ge 2 ]; then
    # An error code equal to or greater than 4 means that some errors
    # could not be corrected.  This requires manual attention, so we
    # offer a chance to try to fix the problem in single-user mode:
    if [ $RETVAL -ge 4 ]; then
      splash -s -u 0 /etc/bootsplash/themes/current/config/verbose-`fbresolution 2>/dev/null`.cfg 2>/dev/null
      echo
      echo
      echo "***********************************************************"
      echo "*** An error occurred during the root filesystem check. ***"
      echo "*** You will now be given a chance to log into the      ***"
      echo "*** system in single-user mode to fix the problem.      ***"
      echo "***                                                     ***"
      echo "*** If you are using the ext2 filesystem, running       ***"
      echo "*** 'e2fsck -v -y <partition>' might help.              ***"
      echo "***********************************************************"
      echo
      echo "Once you exit the single-user shell, the system will reboot."
      echo
      PS1="(Repair filesystem) \#"; export PS1
      sulogin
    else # With an error code of 2 or 3, reboot the machine automatically:
      echo
      echo
      echo "***********************************"
      echo "*** The filesystem was changed. ***"
      echo "*** The system will now reboot. ***"
      echo "***********************************"
      echo
    fi

    echo "Unmounting file systems."
    umount -a -r
    mount -n -o remount,ro /
    echo "Rebooting system."
    sleep 2
    reboot -f
  fi

# Remount the root filesystem in read-write mode
  echo "Remounting root device with read-write enabled."
  /sbin/mount -w -v -n -o remount /
  if [ $? -gt 0 ] ; then
    echo
    echo "Attempt to remount root device as read-write failed!  This is going to"
    echo "cause serious problems."
    echo 
    echo "If you're using the UMSDOS filesystem, you **MUST** mount the root partition"
    echo "read-write!  You can make sure the root filesystem is getting mounted "
    echo "read-write with the 'rw' flag to Loadlin:"
    echo
    echo "loadlin vmlinuz root=/dev/hda1 rw   (replace /dev/hda1 with your root device)"
M    echo
    echo "Normal bootdisks can be made to mount a system read-write with the rdev command:"
    echo
    echo "rdev -R /dev/fd0 0"
    echo
    echo "You can also get into your system by using a boot disk with a command like this"
    echo "on the LILO prompt line:  (change the root partition name as needed)"
    echo 
    echo "LILO: mount root=/dev/hda1 rw"
    echo
    echo "Please press ENTER to continue, then reboot and use one of the above methods to"
    echo -n "get into your machine and start looking for the problem. " 
    read junk; 
  fi

else
  echo "Testing filesystem status: read-write filesystem"
  if cat /etc/fstab | grep ' / ' | grep umsdos 1> /dev/null 2> /dev/null ; then
    ROOTTYPE="umsdos"
  fi

  if [ ! "$ROOTTYPE" = "umsdos" ]; then # no warn for UMSDOS
    splash -s -u 0 /etc/bootsplash/themes/current/config/verbose-`fbresolution 2>/dev/null`.cfg 2>/dev/null
    echo
    echo "*** ERROR: Root partition has already been mounted read-write. Cannot check!"
    echo
    echo "For filesystem checking to work properly, your system must initially mount"
    echo "the root partition as read only. Please modify your kernel with 'rdev' so that"
    echo "it does this. If you're booting with LILO, add a line:"
    echo
    echo "   read-only"
    echo
    echo "to the Linux section in your /etc/lilo.conf and type 'lilo' to reinstall it."
    echo
    echo "If you boot from a kernel on a floppy disk, put it in the drive and type:"
    echo "   rdev -R /dev/fd0 1"
    echo
    echo "If you boot from a bootdisk, or with Loadlin, you can add the 'ro' flag."
    echo
    echo "This will fix the problem *AND* eliminate this annoying message. :^)"
    echo
    echo -n "Press ENTER to continue. "
    read junk;
  fi
fi # Done checking root filesystem

# Any /etc/mtab that exists here is old, so we delete it to start over:
/bin/rm -f /etc/mtab*
# Remounting the / partition will initialize the new /etc/mtab:
/sbin/mount -w -o remount /

#################    /etc/rc.d/rc.udev start  
/etc/rc.d/rc.text 3 'Starting udev'
  progressbar 18
  sh  /etc/rc.d/rc.udev start

##-------------------------------------------------------

# Set the system time from the hardware clock using hwclock --hctosys.
if ! grep -qw nohwclock /proc/cmdline && [ -x /sbin/hwclock ]; then
  if ! grep -q -w rtc /proc/ioports ; then
    CLOCK_OPT="--directisa"
  fi
  if grep "^UTC" /etc/hardwareclock 1> /dev/null 2> /dev/null ; then
    /etc/rc.d/rc.text 9 'Setting system time from the hardware clock (UTC).'
      # bootsplash
      progressbar 18
    echo "Setting system time from the hardware clock (UTC)."
    /sbin/hwclock $CLOCK_OPT --utc --hctosys >> $INITLOG 2>&1
  else
    /etc/rc.d/rc.text 9 'Setting system time from the hardware clock (localtime).'
      # bootsplash
      progressbar 19
    echo "Setting system time from the hardware clock (localtime)."
    /sbin/hwclock $CLOCK_OPT --localtime --hctosys >> $INITLOG 2>&1
  fi
fi
## SWAP and LOCAL FILE SYSTEM
## Start swap and update
/etc/rc.d/rc.text 4 'Activating swap file'
      # bootsplash
      progressbar 20
echoc "Activating swap file" cyan
swapon -a 
#/sbin/update &
swapon -s

##-------------------------------------------------------


#--------------------------------------------------------------------------------

# Initialize the Logical Volume Manager.
# This won't start unless we find /etc/lvmtab (LVM1) or 
# /etc/lvm/backup/ (LVM2).  This is created by /sbin/vgscan, so to
# use LVM you must run /sbin/vgscan yourself the first time (and
# create some VGs and LVs).
if [ -r /etc/lvmtab -o -d /etc/lvm/backup ]; then
      /etc/rc.d/rc.text 4 'Initializing LVM (Logical Volume Manager):'
      # bootsplash
      progressbar 20
  echo "Initializing LVM (Logical Volume Manager)"
  # Check for device-mapper support.
  if ! cat /proc/devices | grep -w device-mapper 1> /dev/null 2> /dev/null ; then
    # If device-mapper exists as a module, try to load it.
    if [ -r /lib/modules/$(cat /proc/sys/kernel/osrelease)/kernel/drivers/md/dm-mod.ko ]; then
      insmod /lib/modules/$(cat /proc/sys/kernel/osrelease)/kernel/drivers/md/dm-mod.ko
    fi
  fi

  # Scan for new volume groups:
  /sbin/vgscan 2> /dev/null
  if [ $? = 0 ]; then
    # This needs a moment to register.
    sleep 10
    # Make volume groups available to the kernel.
    # This should also make logical volumes available.
    /sbin/vgchange -ay
    # Enable swapping again in case any LVs are used for swap.  Ignore previous error.  :-)
    /sbin/swapon -a
  fi
fi

# Check all the non-root filesystems:
if [ ! -r /etc/fastboot ]; then
  /etc/rc.d/rc.text 4 "Checking non-root filesystems"
      # bootsplash
      progressbar 20
  echo "Checking non-root filesystems"
  /sbin/fsck $FORCEFSCK -C -R -A -a 
fi

## mount file systems in fstab (and create an entry for /)
## but not NFS,SAMBA because TCP/IP is not yet configured
      /etc/rc.d/rc.text 5 'Mounting local file systems'
      # bootsplash
      progressbar 25
echo "Mounting local file systems"
/sbin/mount -a -v -t nonfs,nosmbfs,noncpfs,noproc 

## Start ldconfig in the background
#/etc/rc.d/rc.text 5 "Updating local libraries"
      # bootsplash
      progressbar 25
echo "Updating local libraries"
ldconfig &

      

## Attempt to umount and remove any leftover /initrd:
if [ -d /initrd ]; then
  /etc/rc.d/rc.text 5 'Umounting initrd ...'
      # bootsplash
      progressbar 25
  echo "Umounting initrd"
  umount /initrd 2> /dev/null
  rmdir /initrd 2> /dev/null
  blockdev --flushbufs /dev/ram0 2> /dev/null
fi

## Mount tmpfs if requested
if [ -x /etc/rc.d/rc.tmpfs ]; then
      /etc/rc.d/rc.text 6 'Mounting tmpfs'
      # bootsplash
      progressbar 30
  echo "Mounting tmpfs"
  /etc/rc.d/rc.tmpfs
fi

## Syncronizing if we run from MSDOS
if [ "$ROOTTYPE" = "umsdos" ]; then # we need to update any files added in DOS:
  /etc/rc.d/rc.text 6 "Synchronizing UMSDOS directory structure"
      # bootsplash
      progressbar 30
  echo "Synchronizing UMSDOS directory structure:"
  echo "  /sbin/umssync -r99 -v- /"
  /sbin/umssync -r99 -v- /
fi

## Filesystem is done


##--------------------------------------------------------------------------
/etc/rc.d/rc.text 6 "Resetting the system so the game can start "
      # bootsplash
      progressbar 30
echoc "Resetting the system so the game can start " cyan

# Remove undesired things
      /etc/rc.d/rc.text 7 'Removing stale lock, temporary and junk files'
      # bootsplash
      progressbar 35
echo "Removing stale lock, temporary and junk files"


rm -f /etc/nologin /etc/shutdownpid /etc/dhcpc/*.pid /etc/forcefsck /etc/fastboot &> /dev/null

# Remove stale locks and junk files 
rm -f /var/lock/* /var/lock/subsys/* /var/spool/uucp/LCK..* \
   /var/run/utmp /var/run/*pid /var/run/lpd* /var/run/ppp* /var/run/usb/* &> /dev/null
( cd /var/log/setup/tmp && rm -rf * )

rm -f /tmp/.X*lock /tmp/core /core /tmp/menu* /tmp/reply* /tmp/vasm.log &> /dev/null

# Remove stale hunt sockets.
[ -r /tmp/hunt -o -r /tmp/hunt.stats ] && rm -f /tmp/hunt* &> /dev/null

## Ensure basic filesystem permissions sanity.
/etc/rc.d/rc.text 8 'Ensuring basic filesystem permissions sanity' 
      # bootsplash
      progressbar 35
echo "Ensuring basic filesystem permissions sanity" 
chmod 755 /
mkdir -p /tmp/.ICE-unix /tmp/.X11-unix
chmod 1777 /tmp /var/tmp /tmp/.ICE-unix /tmp/.X11-unix

## Looks like we have to create this.
#mkdir -p /var/log/lastlog

## Create a fresh utmp file:
touch /var/run/utmp
chown root.utmp /var/run/utmp
chmod 664 /var/run/utmp

# Launch any script in /var/log/setup/init
# Kinda like Windows  style of "reboot to continue setup" :)
# Hmmm is it a security hole ?
for fsetup in /var/log/setup/init/*; do
    if [ -x $fsetup ]; then
        splash -s -u 0 /etc/bootsplash/themes/current/config/verbose-`fbresolution 2>/dev/null`.cfg 2>/dev/null
	$fsetup
	rm -f $fsetup
        splash -s -u 0 /etc/bootsplash/themes/current/config/bootsplash-`fbresolution 2>/dev/null`.cfg 2>/dev/null
        /etc/rc.d/rc.text 8 "Continuing system initialization"
      # bootsplash
      progressbar 40
        sleep 1
	echoc "Continuing system initialization" cyan
    fi
done

#----------------------------------------------------------------------------
# BASIC HARDWARE
## Very basic hardware, just enough to setup the local filesystem
/etc/rc.d/rc.text 8 'Initializing basic system'
      # bootsplash
      progressbar 40
echocl "Initializing basic system" cyan

# If there's no /etc/hostname, fall back on this default:
if [ ! -r /etc/hostname ]; then
  echo "vector.linux.vnet" > /etc/hostname
fi

# Set the hostname.
HOSTNAME=`cat /etc/hostname`
/etc/rc.d/rc.text 9 "Setting hostname: $HOSTNAME"
      # bootsplash
      progressbar 45
echol "Setting hostname: $HOSTNAME"
/bin/hostname ${HOSTNAME}  >> $INITLOG 2>&1
      
# Carry an entropy pool between reboots to improve randomness.
if [ -f /etc/random-seed ]; then
  /etc/rc.d/rc.text 9 'Using /etc/random-seed to initialize /dev/urandom.'
      # bootsplash
      progressbar 45
  echol "Using /etc/random-seed to initialize /dev/urandom."
  cat /etc/random-seed >/dev/urandom
fi

# Use the pool size from /proc, or 512 bytes:
if [ -r /proc/sys/kernel/random/poolsize ]; then
  dd if=/dev/urandom of=/etc/random-seed count=1 \
     bs=$(cat /proc/sys/kernel/random/poolsize) >> $INITLOG 2>&1
else
  dd if=/dev/urandom of=/etc/random-seed count=1 bs=512 >> $INITLOG 2>&1
fi
chmod 600 /etc/random-seed

## Load a custom keymap
if [ -x /etc/rc.d/rc.keymap ] ; then
  /etc/rc.d/rc.text 10 'Setting up keymap'
      # bootsplash
      progressbar 50
  echol "Setting up keymap"
  /etc/rc.d/rc.keymap  >> $INITLOG 2>&1
fi

## Screen blanks after 15 minutes idle time.
/bin/setterm -blank 0   >> $INITLOG 2>&1

## Hardware is done, for now
## Will do advanced harware initialization on rc.M


# Configure runtime kernel parameters:
if [ -x /sbin/sysctl -a -r /etc/sysctl.conf ]; then
  /etc/rc.d/rc.text 10 "Setting up kernel run time parameters"
      # bootsplash
      progressbar 50
  echol "Setting up kernel run time parameters"
  /sbin/sysctl -e -p /etc/sysctl.conf >> $INITLOG 2>&1
fi

/etc/rc.d/rc.text 10 "Initialization is done"
      # bootsplash
      progressbar 50
#echoc "Startup initialization is done" green


