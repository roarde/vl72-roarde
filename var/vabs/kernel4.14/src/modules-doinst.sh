#!/bin/bash

KVER=@KVER@

# Generate initrd
printf "Generating initrial ram disk... "
mv /boot/initrd.gz /boot/initrd.gz.old
mkinitrd -c -F -k ${KVER} 2>&1 >/dev/null
printf "Done.\n"

# Update the boot menu
printf "Updating boot menu..."
grub-mkconfig -o /boot/grub/grub.cfg 2>&1 >/dev/null
printf "Done.\n"


