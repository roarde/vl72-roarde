#!/bin/sh

# Copyright 2009  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SCRIPT IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SCRIPT, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Slackware build script for groff

NAME="groff"
VERSION=${VERSION:-"1.21"}
ARCH=${ARCH:-"$(uname -m)"}
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"ftp://ftp.gnu.org/gnu/${NAME}/${NAME}-${VERSION}.tar.gz"}

NUMJOBS=${NUMJOBS:--j6}
VL_PACKAGER="M0E-lnx"
CWD=$(pwd)
TMP=${TMP:-/tmp}
PKG=$TMP/package-groff
OUTPUT=$CWD/..

if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
fi

if [ "$NORUN" != 1 ]; then

	
# Download the source code
for src in $(echo $LINK); do
	(
	cd $CWD
	wget -c --no-check-certificate $src 
	)
done

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf $NAME-$VERSION
tar xvf $CWD/$NAME-$VERSION.tar.gz || exit 1
cd groff-$VERSION || exit 1
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Make this thing respect our declared docdir setting
# zcat $CWD/groff.docdir.diff.gz | patch -p1 --verbose || exit 1

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
PAGE=letter ./configure \
  --prefix=/usr \
  --with-appresdir=/usr/lib/X11 \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/$NAME-$VERSION \
  --build=$ARCH-pc-linux || exit 1

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG

( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

( cd src/devices/xditview
  mkdir -p $PKG/etc/X11/app-defaults
  cp -a GXditview.ad $PKG/etc/X11/app-defaults/GXditview
)

mkdir -p $PKG/usr/doc/groff-$VERSION/xditview
cp -a \
  BUG-REPORT COPYING FDL INSTALL INSTALL.gen MORE.STUFF \
  NEWS PROBLEMS PROJECTS README TODO VERSION \
  $PKG/usr/doc/groff-$VERSION
cp -a src/devices/xditview/{README,TODO} $PKG/usr/doc/groff-$VERSION/xditview

# If you want all this stuff, it's in the source tarball:
rm -rf $PKG/usr/doc/groff-$VERSION/*.ps \
  $PKG/usr/doc/groff-$VERSION/examples \
  $PKG/usr/doc/groff-$VERSION/html \
  $PKG/usr/doc/groff-$VERSION/pdf

( cd $PKG/usr/man
  find . -type f -exec gzip -9 {} \;
  for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
)

rm -f $PKG/usr/info/dir
gzip -9 $PKG/usr/info/*

# Do not use color ANSI output by default for man pages.
# A silly "innovation" if ever there was one, sure to break
# a ton of existing scripts otherwise...
zcat $CWD/groff.man.mdoc.local.gz >> $PKG/usr/share/groff/site-tmac/man.local
zcat $CWD/groff.man.mdoc.local.gz >> $PKG/usr/share/groff/site-tmac/mdoc.local

( cd $PKG/usr/bin
  rm -rf geqn ; ln -sf eqn geqn
  rm -rf gindxbib ; ln -sf indxbib gindxbib
  rm -rf gpic ; ln -sf pic gpic
  rm -rf grefer ; ln -sf refer grefer
  rm -rf gsoelim ; ln -sf soelim gsoelim
  rm -rf zsoelim ; ln -sf soelim zsoelim
  rm -rf gtbl ; ln -sf tbl gtbl
  rm -rf gtroff ; ln -sf troff gtroff
  rm -rf glookbib ; ln -sf lookbib glookbib
  rm -rf gnroff ; ln -sf nroff gnroff
  rm -rf gneqn ; ln -sf neqn gneqn
)

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/slack-desc > $OUTPUT/slack-desc

cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $OUTPUT $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
/sbin/makepkg -l y -c n $OUTPUT/$NAME-$VERSION-$ARCH-$BUILD.txz

fi
