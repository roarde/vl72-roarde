#!/bin/sh

TESTFILE=/tmp/clang-test-$$.c
TEST_STRING="Hello World!"
cat >>$TESTFILE <<EOF
#include <stdio.h>

int main() {
    printf("Hello World!\n");
    return 0;
}
EOF
passed() {
printf "PASSED\n"
}
failed() {
printf "FAILED\n"
}

printf "Compile a test program with clang..."
clang $TESTFILE -o /tmp/hello || { failed; exit 1; }
passed
printf "Compile a C file into an LLVM bitcode file..."
clang -O3 -emit-llvm $TESTFILE -c -o /tmp/hello.bc || { failed; exit 1; }
passed
printf "Run the compiled program..."
cres="$(/tmp/hello)" || { failed; exit 1; }
passed
printf "Testing results produced by the executed program..."
if [[ "$cres" != $TEST_STRING ]]; then
	failed
	exit 1
fi
passed
printf "Execute the llvm bitcode program..."
bcres="$(lli /tmp/hello.bc)" || { failed; exit 1; }
passed
printf "Testing results produced by the llvm bitcode program..."
if [[ "$bcres" != $TEST_STRING ]]; then
	failed
	exit 1
fi
passed
printf "Compile the program to native assembly using the LLC code generator ... "
llc /tmp/hello.bc -o /tmp/hello.s || { failed; exit 1; }
passed
printf "Assemble the native assembly language file into a program ... "
gcc /tmp/hello.s -o /tmp/hello.native || { failed; exit 1; }
passed
printf "Execute the native code program and test the results ..."
nres="$(/tmp/hello.native)" || { printf "Program failed to run... FAILED!\n"; exit 1; }
if [[ "$nres" != $TEST_STRING ]]; then
	failed; exit 1
fi
passed


