#!/usr/bin/bash
# This script assumes it will be launched within "/NAME/VERSION/src" dir.
# With all sources in "src" Your Vector Linux .tlz package, slack-desc,
# and slack-required will be found in "VERSION" dir. The extraction and
# build will be in a temp dir created in "NAME" dir, and then removed on exit.
# Comment out second to last line to keep this dir intact.


NAME="perl"            #Enter package Name!
VERSION=${VERSION:-"5.22.0"}      #Enter package Version!
VL_PACKAGER=${VL_PACKAGER:-"Uelsk8s"}   #Enter your Name!
LINK=${LINK:-"http://www.cpan.org/src/perl-$VERSION.tar.bz2"} #Enter URL for package here!


#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-$(uname -m)}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
MAKEDEPENDS="!perl-xml-parser" # Mysql needed for building the DBD-mysql extension

#----------------------------------------------------------------------------

if [ "$NORUN" != 1 ]; then

#SETUP PACKAGING ENVIRONMENT
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p perl-modules
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME
#--------------------------------------------


if [ $UID != 0 ]; then
   echo "You are not authorized to run this script. Please login as root"
   exit 1
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
   echo "Requiredbuilder not installed, or not executable."
   exit 1
fi

if [ $VL_PACKAGER = "YOURNAME" ]; then
   echo 'Who are you?
   Please edit VL_PACKAGER=${VL_PACKAGER:-YOURNAME} in this script.
   Change the word "YOURNAME" to your VectorLinux packager name.
   You may also export VL_PACKAGER, or call this script with
   VL_PACKAGER="YOUR NAME HERE"'
   exit 1
fi


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

if [ -x /usr/bin/perl ]; then
  echo "Perl detected."
  echo
  echo "It's a good idea to remove your existing perl first."
  echo
  echo "We are removing Perl NOW!!"
  echo
  sleep 15
  removepkg perl
fi

#GET THE SOURCE
#--------------------------------------------
count=0
for SRC in $(echo $LINK);do
  if [ $count = 1 ];then
    cd perl-modules
  fi
  if [ ! -f $(basename $SRC) ];then
    wget -c $SRC || exit 1
  fi
  ((count++))
done
cd $CWD
#--------------------------------------------

rm -rf $PKG
mkdir -p $PKG
cd $TMP
rm -rf $NAME-$VERSION


#EXTRACT SOURCES
#-----------------------------------------------------
echo "Extracting source..."
tar xf $CWD/$NAME-$VERSION.tar.* --checkpoint=10000 || exit 1

cd $TMP/$NAME-$VERSION

# Put any Patches here
#-----------------------------------------------------
for i in $CWD/patches/*;do
  echo $i 
  patch -p1 <$i 
done
#-----------------------------------------------------

#SET PERMISSIONS
#-----------------------------------------
echo "Setting permissions..."
chown -R root:root .
find . -perm 664 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 2777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 2755 -exec chmod 755 {} \;
find . -perm 774 -exec chmod 644 {} \;
find . -perm 666 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
#-----------------------------------------

# If after all this time you still don't trust threads, comment
# out the variable below:
#
USE_THREADS="-Dusethreads -Duseithreads"


#CONFIGURE & MAKE
#----------------------------------------------------------------------
# If you are building a KDE-related app, then change the following
# arguments in the script below:
# --prefix=$(kde-config -prefix) 
# --sysconfdir=/etc/kde 
#
# Making these changes will ensure that your package will build in the
# correct path and that it will work seamlessly within the KDE environment.
#
#-----------------------------------------------------------------------

# Install necessary module deps from CPAN
export PERL_MM_USE_DEFAULT=1
echo "Configuring source..."
# Configure perl:
./Configure -de \
  -Dprefix=/usr \
  -Dvendorprefix=/usr \
  -Dcccdlflags='-fPIC' \
  -Dinstallprefix=/usr \
  -Dlibpth="/usr/lib${LIBDIRSUFFIX} /lib${LIBDIRSUFFIX}" \
  -Doptimize="$SLKCFLAGS" \
  -Dusemultiplicity \
  -Duseshrplib \
  $USE_THREADS \
  -Dpager='/usr/bin/less -isr' \
  -Dinc_version_list='5.12.3 5.12.2 5.12.1 5.12.0 5.10.1 5.10.0 5.8.8 5.8.7 5.8.6 5.8.5 5.8.4 5.8.3 5.8.2 5.8.1 5.8.0' \
  -Dcf_by="${SLKDIST}" \
  -Duseshrplib \
  -Ubincompat5005 \
  -Darchname=$ARCH-linux || exit 1

# -Duseshrplib creates libperl.so
# -Ubincompat5005 helps create DSO -> libperl.so
  
# Kludge for gcc-4.2.4's needlessly changed output:
cat makefile | grep -v '\<command-line\>' > foo
mv foo makefile
cat x2p/makefile | grep -v '\<command-line\>' > foo
mv foo x2p/makefile

# Build perl
make $NUMJOBS || exit 1
make test

# Install perl package:
make install DESTDIR=$PKG || exit 1
make install # Needed for required-builder to work
mkdir -p $PKG/usr/lib/perl5/vendor_perl/${VERSION}/${ARCH}-linux-thread-multi || exit 1

mkdir -p $PKG/usr/doc/$NAME-$VERSION
# Install documentation
cp -a \
  AUTHORS Artistic Copying INSTALL MANIFEST README README.Y2K README.cn README.jp README.ko README.micro README.tw Todo.micro \
  $PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

#----------------------------------------------------------------------

# We follow LSB with symlinks in /usr/share:
( cd $PKG/usr/share
  mv man .. )
( cd $PKG/usr/man/man1
  mkdir foo
  cp *.1 foo
  rm *.1
  mv foo/* .
  rmdir foo
  gzip -9 *
  ln -sf c2ph.1.gz pstruct.1.gz
  ln -sf s2p.1.gz psed.1.gz )
( cd $PKG/usr/man/man3
  gzip -9 * )

chmod 755 $PKG/usr/bin/*
chmod 644 $PKG/usr/man/man?/*
rmdir $PKG/usr/share

# This file shouldn't get clobbered:
if [ -r $PKG/usr/lib/perl5/${VERSION}/${ARCH}-linux-thread-multi/perllocal.pod ]; then
  mv $PKG/usr/lib/perl5/${VERSION}/${ARCH}-linux-thread-multi/perllocal.pod $PKG/usr/lib/perl5/${VERSION}/${ARCH}-linux-thread-multi/perllocal.pod.new
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh

mkdir -p $PKG/install

#STRIPPING
#------------------------------------------------------------------------------------------------------------------
cd $PKG
echo " "
echo "Stripping...."
echo " "
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#------------------------------------------------------------------------------------------------------------------


#FINISH PACKAGE
#--------------------------------------------------------------
echo "Finding dependencies..."
requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
#--------------------------------------------------------------
fi
