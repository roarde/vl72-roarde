#!/bin/bash

# Test the go package with a test program.
CWD=$PWD
TFILE=/tmp/hello-world.go

cat >$TFILE <<EOF
// Sample go file.
package main
import "fmt"
func main() {
	fmt.Println("Hello, World")
}

EOF

printf "Verify go version reported by binary..."
NORUN=1 . $CWD/go.SlackBuild
GOVER=$(go version |cut -f 3 -d ' '|sed 's|go||g')
if [ "x$GOVER" != "x${VERSION}" ]; then
	printf "${GOVER} ... FAIL!\n"
else
	printf "${GOVER} ... PASS\n"
fi

printf "Compile/run test 'go' program...."
go run $TFILE >  ${TFILE}.out || { printf "FAIL\n"; exit 1; }
printf "PASS\n"

retString="$(cat ${TFILE}.out)"
printf "Test results of of executed sample program..."
if [ "$retString" != "Hello, World" ]; then
	printf "FAIL!\n"
	exit 1
else
	printf "PASS\n"
fi
