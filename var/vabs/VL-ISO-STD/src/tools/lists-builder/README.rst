PACKAGE LISTS GENERATOR
=======================

This script will generate an updated list of packages that will be used
to build the ISO image.


INTENDED USE
------------

The intended use of this script is as follows.

#.  Install the CL ISO in a VM or otherwise isolated environment.
#.  Install all the required packages to build that CL installation
    into a STD desktop.
#.  Place the ``pkgs`` file and ``veclinux`` directory from the *previous*
    released ISO (final) into the ``$OLD_DATA`` directory (see script).
#.  Run the script.

Upon execution, the script will analize what is installed in your current
system, and search for those packages in the ``$OLD_DATA`` definitions.  
When a package name is matched, it will copy that entry in the ``$NEW_DATA`` 
directory in the matching file.  

When a package is not found in the old lists ( but you have it installed ), 
the script will list these in ``$NEW_DATA/pkgs_to_locate_in_iso_dirs`` and
the same goes for $NEW_DATA/veclinux/PKGS_TO_LOCATE_IN_BULKS``.  The entries
on these files must be manually appended with the desired leading directory 
in the ``pkgs`` file and be appended to the correct bulk definition file in
the ``veclinux`` directory.

The script will also pring warnings about packages being listed more than
once in the bulk definition files.  These entries can be found in the
``$NEW_DATA/veclinux/DUPLICATED_ENTRIES`` file.  This file is purely for
information only... The duplicates are automatically removed on the new
data.


CLEANING UP
-----------

After adjusting the ``pkgs`` file and the bulk definition files under the
``veclinux`` directory, these files **must be deleted**

- pkgs_to_locate_in_iso_dires
- veclinux/PKGS_TO_LOCATE_IN_BULKS
- veclinux/DUPLICATED_ENTRIES

.. note::
	**DO NOT** push any of these files to the git source with the
	iso build sources
